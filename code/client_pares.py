import sys

import grpc
import services_pb2_grpc
from inputtypes import InputTypes

# Cliente para o servidor de pares
if __name__ == "__main__":
  if len(sys.argv) >= 2:
    # Lendo os comandos da entrada padrão
    commands = map(InputTypes.from_string_pares, sys.stdin)
    
    # Criando a conexão com o identificador dado na linha de comando
    with grpc.insecure_channel(sys.argv[1]) as channel:
      stub = services_pb2_grpc.ParesStub(channel)

      # Enviando cada mensagem indicada na entrada padrão para o servidor conectado
      for cmd in commands:
        if cmd is not None:
          cmd.print(cmd.send(stub))