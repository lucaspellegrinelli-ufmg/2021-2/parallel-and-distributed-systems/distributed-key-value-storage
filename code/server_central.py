import sys
from concurrent import futures

import threading
import grpc
import services_pb2
import services_pb2_grpc

# A variável "storage" serve como a variável de armazenamento de onde cada chave está armazenada (em qual servidor).
storage = dict()

class Central(services_pb2_grpc.CentralServicer):
  # Entrada:
  # - stop_event (threading.Event): Evento para finalizar o servidor
  def __init__(self, stop_event):
    self.stop_event = stop_event

  # Método que controla a funcionalidade da função de "Registro".
  # Recebe um identificador de um servidor e uma lista de chaves. Assim guardamos na variável "storage" que o servidor
  # (identificador) possuí todos os dados referentes as chaves especificadas.
  def Register(self, request, context):
    for key in request.keys:
      storage[key] = request.identifier

    return services_pb2.RegisterResponse(count=len(request.keys))

  # Método que controla a funcionalidade da função de "Mapeamento".
  # Recebe uma chave de um dado e devemos retornar em qual servidor (identificador) aquele dado se encontra para que então
  # o cliente possa acessa-lo e buscar o dado em si. Caso não soubermos onde aquele dado está, retornamos uma string vazia.
  def Map(self, request, context):
    if request.key in storage:
      return services_pb2.MapResponse(identifier=storage[request.key])
    else:
      return services_pb2.MapResponse(identifier="")
  
  # Método que controla a funcionalidade da função "Término".
  # Essa funcão apenas ativa o gatilho da variável "stop_event" que controla quando o método principal
  # (main) pode parar de executar esse servidor. Isso é importante para que o cliente ainda receba a
  # resposta dessa função, o que não seria o caso se apenas interrompessemos a execução desse arquivo.
  def Finish(self, request, context):
    self.stop_event.set()
    return services_pb2.FinishResponse(count=len(storage))

# Método principal. Aqui lemos os valores da linha de comando assim como iniciamos o servidor Central
if __name__ == "__main__":
  if len(sys.argv) >= 2:   
    # Iniciando o servidor na porta correta
    stop_event = threading.Event()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    services_pb2_grpc.add_CentralServicer_to_server(Central(stop_event), server)
    server.add_insecure_port(f"[::]:{sys.argv[1]}")
    server.start()
    stop_event.wait()
    server.stop(0)