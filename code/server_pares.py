import sys
from concurrent import futures
import threading
import grpc
import socket

import services_pb2
import services_pb2_grpc

from inputtypes import RegisterInput

# A variável "storage" serve como a variável de armazenamento das chaves.
storage = dict()

class Pares(services_pb2_grpc.ParesServicer):
  # Entrada:
  # - stop_event (threading.Event): Evento para finalizar o servidor
  # - port (str): Porta onde o servidor deve ser instanciado
  # - flag (boolean): Se a flag que controla a versão do método "Activation" está presente ou não
  def __init__(self, stop_event, port, flag):
    self.stop_event = stop_event
    self.port = port
    self.flag = flag

  # Método que controla a funcionalidade da função de "Inserção".
  # Recebe uma chave e um valor e coloca esse par no armazenamento. Caso essa seja uma nova entrada,
  # retornamos o valor 0, caso contrário retornamos o valor -1.
  def Insert(self, request, context):
    if request.key not in storage:
      storage[request.key] = request.value
      return services_pb2.InsertResponse(value=0)
    else:
      return services_pb2.InsertResponse(value=-1)

  # Método que controla a funcionalidade da função de "Consulta".
  # Recebe uma chave e retorna o valor atrelado a essa chave no armazenamento. Caso essa chave não
  # exista no armazenamento, retornamos "None".
  def Query(self, request, context):
    if request.key in storage:
      return services_pb2.QueryResponse(value=storage[request.key])
    else:
      return services_pb2.QueryResponse(value=None)

  # Método que controla a funcionalidade da função de "Ativação".
  # Recebe um identificador e:
  #  - Na parte 1, apenas retorna o valor 0
  #  - Na parte 2, reporta para o servidor central (utilizando a função de "Registro") as chaves
  # que esse servidor tem no armazenamento. Assim, esse método serve como um gatilho para atualizar
  # o servidor central de quais dados esse servidor tem. O valor de retorno é a quantidade de chaves
  # reportadas ao servidor central.
  def Activation(self, request, context):
    if self.flag:
      # Cria a conexão com o servidor central
      with grpc.insecure_channel(request.identifier) as channel:
        stub = services_pb2_grpc.CentralStub(channel)

        # Obtem o identificador desse servidor
        identifier = f"{socket.getfqdn()}:{port}"

        # Cria e envia a mensagem de "Registro" com as chaves do armazenamento
        cmd = RegisterInput(identifier, storage.keys())
        cmd.send(stub)
      return services_pb2.ActivationResponse(value=len(storage))
    else:
      return services_pb2.ActivationResponse(value=0)
      
  # Método que controla a funcionalidade da função "Término".
  # Essa funcão apenas ativa o gatilho da variável "stop_event" que controla quando o método principal
  # (main) pode parar de executar esse servidor. Isso é importante para que o cliente ainda receba a
  # resposta dessa função, o que não seria o caso se apenas interrompessemos a execução desse arquivo.
  def Finish(self, request, context):
    self.stop_event.set()
    return services_pb2.FinishResponse(count=0)

# Método principal. Aqui lemos os valores da linha de comando assim como iniciamos o servidor de Pares
if __name__ == "__main__":
  if len(sys.argv) >= 2:
    # Lendo a porta e a flag da linha de comando
    flag = len(sys.argv) > 2
    port = sys.argv[1]

    # Iniciando o servidor na porta correta
    stop_event = threading.Event()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    services_pb2_grpc.add_ParesServicer_to_server(Pares(stop_event, port, flag), server)
    server.add_insecure_port(f"[::]:{port}")
    server.start()
    stop_event.wait()
    server.stop(0)