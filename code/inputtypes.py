import socket
import grpc
import services_pb2
import services_pb2_grpc

# Classe abstrata que é a super classe de objetos que representam cada um dos tipos de mensagem
# que podemos enviar aos dois tipos de servidor
class InputTypes:
  # Método abstrato que diz deve implementar o envio da mensagem por meio do stub da conexão
  def send(self, stub):
    raise NotImplementedError()

  # Método que deve enviar na saida padrão a mensagem correta dado a resposta da mensagem enviada pelo
  # método "send"
  def print(self, response):
    raise NotImplementedError()

  # Método estático auxiliar para processamento da entrada padrão. A partir das linhas da entrada padrão
  # referentes aos comandos que o cliente deve executar, ele cria os objetos corretos para cada ação com
  # os parâmetros corretos.
  @staticmethod
  def from_string_pares(str):
    parts = [x.strip() for x in str.strip().split(",")]
    if parts[0] == "I":
      return InsertInput(key=int(parts[1]), value=parts[2])
    elif parts[0] == "C":
      return QueryInput(key=int(parts[1]))
    elif parts[0] == "A":
      return ActivationInput(identifier=parts[1])
    elif parts[0] == "T":
      return FinishInput()
    return None

  # Análogo ao método "from_string_pares" porém referente ao servidor central
  @staticmethod
  def from_string_central(str):
    parts = [x.strip() for x in str.strip().split(",")]
    if parts[0] == "C":
      return MapInput(key=int(parts[1]))
    elif parts[0] == "T":
      return FinishInput()
    return None

# Classe referente a ação de "Inserir" no servidor de pares
class InsertInput(InputTypes):
  def __init__(self, key, value):
    self.key = key
    self.value = value

  def send(self, stub):
    return stub.Insert(services_pb2.InsertRequest(key=self.key, value=self.value))

  def print(self, response):
    print(response.value)

# Classe referente a ação de "Consultar" no servidor de pares
class QueryInput:
  def __init__(self, key):
    self.key = key

  def send(self, stub):
    return stub.Query(services_pb2.QueryRequest(key=self.key))

  def print(self, response):
    print(response.value)

# Classe referente a ação de "Ativação" no servidor de pares
class ActivationInput:
  def __init__(self, identifier):
    self.identifier = identifier

  def send(self, stub):
    return stub.Activation(services_pb2.ActivationRequest(identifier=self.identifier))

  def print(self, response):
    print(response.value)

# Classe referente a ação de "Registro" no servidor central
class RegisterInput:
  def __init__(self, identifier, keys):
    self.identifier = identifier
    self.keys = keys

  def send(self, stub):
    return stub.Register(services_pb2.RegisterRequest(identifier=self.identifier, keys=self.keys))

  def print(self, response):
    return

# Classe referente a ação de "Mapeamento" no servidor central
class MapInput:
  def __init__(self, key):
    self.key = key

  def send(self, stub):
    return stub.Map(services_pb2.MapRequest(key=self.key))

  # No caso do mapeamento, devemos escrever na saida padrão o resultado da consulta ao servidor
  # de pares correspondente. Dessa forma, aqui criamos uma conexão com o identificador do servidor
  # de pares obtido pelo servidor cental e fazemos a consulta do valor para então manda-lo na
  # saída padrão.
  def print(self, response):
    if len(response.identifier) > 0:
      print(f"{response.identifier}:", end="")

      # Conexão com o servidor de pares que tem o valor que queremos
      host, port = response.identifier.split(":")
      address = socket.gethostbyname(host)
      with grpc.insecure_channel(f"{address}:{port}") as channel:
        stub = services_pb2_grpc.ParesStub(channel)

        # Criação e execução da ação de consulta
        command = QueryInput(key=self.key)
        command.print(command.send(stub))
    else:
      print("")

# Classes referentes a ação de "Término" em ambos servidores
class FinishInput:
  def send(self, stub):
    return stub.Finish(services_pb2.FinishRequest())

  def print(self, response):
    print(response.count)